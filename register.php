<?php

/* Register Successful Message and Link to Login Page */



session_start();

// Validating access without login
if (!isset($_SESSION['regaccess'])) {

    header('Location: signup.php');

    exit();
}

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Welcome User</title>
    <link rel="stylesheet" href="assets/style/index.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
</head>

<body>
    <div class="alert alert-success" role="alert" align="center">
        <h1>You are Successfully Registered!</h1>
        <a href="index.php">Login Page</a>
    </div>

</body>

</html>