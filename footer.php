<footer>
    <nav class="navbar  navbar-dark bg-dark">
        <!-- Navbar content -->
        <div class="contact">
            <a class="navbar-brand">
                <h5>Contacts</h5>
                <p><i class="fas fa-envelope"> Email: rohit3644@gmail.com</i></p>
                <i class="fas fa-phone-square-alt"> Phone: 9938741890</i>
            </a>
        </div>
        <ul class="navbar-nav">
            <a class="navbar-brand">
                <h5>Links</h5>
            </a>
            <nav class="nav flex-column">
                <a class="nav-link active" href="profile.php">Profile</a>
                <a class="nav-link active" href="welcome.php">Welcome</a>
            </nav>
        </ul>
        <!-- About the company -->
        <a class="navbar-brand">
            We are an award-winning creative agency,<br /> dedicated to the best result in web development <br /> and machine learning.
            <br><br><i class="far fa-copyright"> Waves.All Rights Reserved</i>
        </a>
    </nav>
</footer>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
</body>

</html>