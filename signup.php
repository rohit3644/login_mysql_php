<?php


/* Registering new User */


session_start();
require_once("src/classes/User.php");
require_once("src/database/database.php");
$obj = Database::getInstance();
$mysqli = $obj->getConnection();
$username = $password = $confirm_password = "";

// On form submit

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $username = check_input($_POST["username"]);
    $password = $_POST["password"];
    $confirm_password = $_POST["passwordc"];
    $_SESSION["regaccess"] = 1;


    $userobj = new User();
    $validate = $userobj->registration($username, $password, $confirm_password, $obj, $mysqli);
    if ($validate == 1) {
        header("Location: register.php");
    } else if ($validate == -1) {
        $msg = "Please enter correct password!";
    } else if ($validate == 0) {
        $errormsg = "Enter Username and Password!";
    } else {
        $msg = "User Exists!";
    }
}


// Data Sanitization

function check_input($data)
{
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);

    return $data;
}

mysqli_close($mysqli);
?>


<!DOCTYPE html>

<html>

<head>

    <title>Registration Page</title>

    <link rel="stylesheet" href="assets/style/index.css">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-1/css/all.min.css" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            window.setInterval(function() {
                $("#username").blur(function() {

                    var username = $(this).val().trim();

                    if (username != '') {

                        $.ajax({
                            url: 'src/database/ajaxdb.php',
                            type: 'post',
                            data: {
                                username: username
                            },
                            success: function(response) {

                                $('#uname_response').html(response).css({
                                    "text-align": "center",
                                    "font-size": "15px"
                                });

                            }
                        });
                    } else {
                        $("#uname_response").html("");
                    }

                });
            }, 2000);
        });
    </script>
</head>

<body>
    <div class="container h-100">
        <div class="d-flex justify-content-center h-100">
            <div class="user_card">
                <div class="d-flex justify-content-center">
                    <div class="brand_logo_container">
                        <img src="https://cdn.freebiesupply.com/logos/large/2x/pinterest-circle-logo-png-transparent.png" class="brand_logo" alt="Logo">
                    </div>
                </div>

                <!-- Form Container -->

                <div class="d-flex justify-content-center form_container">
                    <form method="post">
                        <div class="input-group mb-3">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                            </div>
                            <input type="text" name="username" class="form-control input_user" value="" placeholder="username" id="username">
                        </div>
                        <div id="uname_response"></div>
                        <div class="input-group mb-2">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="fas fa-key"></i></span>
                            </div>
                            <input type="password" name="password" class="form-control input_pass" value="" placeholder="password">
                        </div>
                        <div class="input-group mb-2">
                            <div class="input-group-append">
                                <span class="input-group-text"><i class="fas fa-key"></i></span>
                            </div>
                            <input type="password" name="passwordc" class="form-control input_pass" value="" placeholder="Confirm password">
                        </div>
                        <div class="d-flex justify-content-center mt-3 login_container">
                            <button type="submit" name="button" class="btn login_btn">Register</button>
                        </div>
                    </form>
                </div>
                <b>
                    <p id="error" align="center"><?php echo $errormsg; ?></p>
                    <p id="error1" align="center"><?php echo $msg; ?></p>
                </b>
            </div>
        </div>
    </div>
</body>
<script>
    // Error Message will be displayed for only 2 seconds
    setTimeout(function() {
        document.getElementById('error').style.display = 'none';
    }, 2000);
    setTimeout(function() {
        document.getElementById('error1').style.display = 'none';
    }, 2000);
</script>

</html>