<?php

// header

require_once("header.php");

session_start();

// Validating access without login

if (!isset($_SESSION['access'])) {
    header('Location: index.php');
    exit();
}

?>

<div class="main-content-welcome" align="center">
    <div class="card">
        <div class="jumbotron">
            <h1 class="display-4">Welcome, <?php echo $_SESSION["username"]; ?></h1>
            <p><a href="profile.php" class="btn btn-info">Profile</a></p>
        </div>
    </div>
</div>

<?php require_once("footer.php"); ?>