<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-1/css/all.min.css" />
    <link rel="stylesheet" href="assets/style/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        $(document).ready(function() {
            $("#name").keyup(function() {
                var name = $(this).val();
                if (!name.match(/^[a-zA-Z ]*$/)) {
                    $("#name_error").html("Name can contain only letters").css({
                        "display": "block",
                        "color": "red",
                        "font-size": "15px"
                    });
                } else {
                    $("#name_error").hide();
                }
            });
            $("#email").blur(function() {
                var email = $(this).val();
                if ($.trim(email) == "") {
                    $("#email_error").hide();
                } else if (!email.match(/^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/)) {
                    $("#email_error").html("Invalid email format").css({
                        "display": "block",
                        "color": "red",
                        "font-size": "15px"
                    });
                } else {
                    $("#email_error").hide();
                }
            });
            $("#age").blur(function() {
                var age = $(this).val();
                if (!(age >= 20 && age <= 30)) {
                    $("#age_error").html("Please enter an age betweem 20 and 30").css({
                        "display": "block",
                        "color": "red",
                        "font-size": "15px"
                    });
                } else {
                    $("#age_error").hide();
                }
            });
        });
    </script>
</head>

<body>
    <nav class="navbar navbar-expand-md bg-dark navbar-dark">
        <!-- Brand -->
        <a class="navbar-brand" href="welcome.php">
            <h1><i class="fab fa-amazon-pay"></i></h1>
        </a>
        <!-- Toggler/collapsibe Button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
            <span class="navbar-toggler-icon"></span>
        </button>
        <!-- Navbar links -->
        <div class="collapse navbar-collapse" id="collapsibleNavbar">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="profile.php">Profile</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="welcome.php">Welcome</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="logout.php">LogOut</a>
                </li>
            </ul>
        </div>
    </nav>