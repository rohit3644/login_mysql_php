<?php

/* LOGIN FUNCTIONALITY AND VALIDATION USING DATABASE */

session_start();

$username = $password = "";
require_once("src/classes/User.php");
require_once("src/database/database.php");
$obj = Database::getInstance();
$mysqli = $obj->getConnection();

// On form submit

if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $username = check_input($_POST["username"]);
  $password = $_POST["password"];
  $_SESSION["access"] = 1;
  $_SESSION["username"] = $username;

  /*  Creating User class object and calling validate function
      which is validating user login and returning 1 if validation is
      successful else returning -1 or 0 and corresponding to below messages
  */

  $userobj = new User();
  $validate = $userobj->validate($username, $password, $obj, $mysqli);
  if ($validate == 1) {
    header("Location: welcome.php");
  } else if ($validate == -1) {
    $errormsg = "Invalid username or password!";
  } else {
    $errormsg = "Enter Username and Password!";
  }
}

// Data Sanitization (For cleaning the user data)

function check_input($data)
{
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);

  return $data;
}

mysqli_close($mysqli);
?>


<!DOCTYPE html>

<html>

<head>

  <title>Login Page</title>

  <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-1/css/all.min.css" />
  <link rel="stylesheet" href="assets/style/index.css">
</head>

<body>
  <div class="container h-100">
    <div class="d-flex justify-content-center h-100">
      <div class="user_card">
        <div class="d-flex justify-content-center">
          <div class="brand_logo_container">
            <img src="https://cdn.freebiesupply.com/logos/large/2x/pinterest-circle-logo-png-transparent.png" class="brand_logo" alt="Logo">
          </div>
        </div>

        <!-- Form Container -->

        <div class="d-flex justify-content-center form_container">
          <form method="post">
            <div class="input-group mb-3">
              <div class="input-group-append">
                <span class="input-group-text"><i class="fas fa-user"></i></span>
              </div>
              <input type="text" name="username" class="form-control input_user" value="" placeholder="username" id="username">
            </div>
            <div class="input-group mb-2">
              <div class="input-group-append">
                <span class="input-group-text"><i class="fas fa-key"></i></span>
              </div>
              <input type="password" name="password" class="form-control input_pass" value="" placeholder="password" id="password">
            </div>
            <div class="d-flex justify-content-center mt-3 login_container">
              <button type="submit" name="button" class="btn login_btn">Login</button>
            </div>
          </form>
        </div>
        <p>
          <div class="register">
            New User?<a href="signup.php">Create an account</a>
          </div>
        </p>
        <b>
          <p id="error" align="center"><?php echo $errormsg; ?></p>
        </b>
      </div>
    </div>
  </div>
</body>
<script>
  // Displaying the error message for 2 seconds
  setTimeout(function() {
    document.getElementById('error').style.display = 'none';
  }, 2000);
</script>

</html>