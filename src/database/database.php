<?php

/// Singleton class for database Connection and INSERT and SELECT query creation

class Database
{
    private static $connection = null;
    private $conn;
    private $servername = "localhost";
    private $username = "root";
    private $password = "Rohit@3644";
    private $database = "UserDetails";
    private function __construct()
    {
        mysqli_report(MYSQLI_REPORT_ERROR | MYSQLI_REPORT_STRICT);
        $this->conn = new mysqli($this->servername, $this->username, $this->password, $this->database);

        if ($this->conn->connect_error) {
            die("Connection failed:" . $this->conn->connect_error);
        }
    }

    public static function getInstance()
    {
        if (self::$connection == null) {
            self::$connection = new Database();
        }

        return self::$connection;
    }

    public function getConnection()
    {
        return $this->conn;
    }
    public function query($query)
    {
        return mysqli_query($this->conn, $query);
    }

    // Function for INSERT QUERY formation
    public function insert($table, $data, $return_flag)
    {
        $result = 0;
        $data_key_string = implode(", ", array_keys($data));
        $count = count(array_keys($data));
        $data_value_array = array_values($data);
        $questionmark_string = "";
        $data_value = array_values($data);
        /* Making a string of question marks for prepared Statements 
        */
        for ($i = 0; $i < $count; $i++) {
            if ($i == $count - 1) {
                $questionmark_string = $questionmark_string . "?";
            } else {
                $questionmark_string = $questionmark_string . "?,";
            }
        }
        // Preparing,binding and executing the query
        $sql = $this->conn->prepare("INSERT INTO " . $table . " (" . $data_key_string . ") VALUES (" . $questionmark_string . ")");
        $bind_type_string = "";
        foreach ($data_value as $val) {
            if (gettype($val) == "integer") {
                $bind_type_string = $bind_type_string . "i";
            } else if (gettype($val) == "string") {
                $bind_type_string = $bind_type_string . "s";
            }
        }
        $sql->bind_param($bind_type_string, ...$data_value_array);
        $sql->execute();
        if ($return_flag == true) {
            $UserId = mysqli_insert_id($this->conn);
            $result = $UserId;
        } else {
            $result = 1;
        }
        return $result;
    }


    // Function for SELECT QUERY formation
    public function select($data, $return_flag, $jointype = null)
    {
        $result_return = "";
        $select_column_string = implode(", ", $data[1]);
        $sql = "SELECT " . $select_column_string . " FROM ";
        $tables_string = implode(" " . $jointype . " ", $data[0]);

        // If query needs joining we concat ON else we concate WHERE

        if ($jointype == null) {
            $sql = $sql . $tables_string . " WHERE";
        } else {
            $sql = $sql . $tables_string . " ON";
        }
        $count = count($data[3]);
        $i = 0;
        $where_condition_value = array_values($data[2]);
        // if query needs joining pass parameter and value else pass parameter and ?
        if ($jointype != null) {
            foreach ($data[2] as $param => $value) {
                $sql = $sql . " " . $param . "= " . $value . " ";
                if ($i < $count) {
                    $sql = $sql . $data[3][$i] . " ";
                    $i++;
                }
            }
        } else if ($jointype == null) {
            foreach ($data[2] as $param => $value) {
                $sql = $sql . " " . $param . "=" . " ?" . " ";
                if ($i < $count) {
                    $sql = $sql . $data[3][$i] . " ";
                    $i++;
                }
            }
        }
        // Preparing,binding and executing the query
        $value = array_values($data[2]);
        $bind_type_string = "";
        foreach ($value as $val) {
            if (gettype($val) == "integer") {
                $bind_type_string = $bind_type_string . "i";
            } else if (gettype($val) == "string") {
                $bind_type_string = $bind_type_string . "s";
            }
        }
        $query = $this->conn->prepare($sql);
        if ($jointype === null) {
            $query->bind_param($bind_type_string, ...$where_condition_value);
        }
        $query->execute();
        if ($return_flag == false) {
            $result = $query->get_result();
            $result_return = $result;
        } else {
            $result_return = $query;
        }
        return $result_return;
    }
}
