<?php

require_once("database.php");
$obj = Database::getInstance();
$mysqli = $obj->getConnection();


if (isset($_POST['username'])) {
    $username = $_POST['username'];
    $data = [
        $tables_array = ["Login"],
        $select_columns_array = ["Id"],
        $where_condition_array = ["UserName" => $username],
        $operator_condition_array = []
    ];

    try {
        $result = $obj->select($data, false);
    } catch (Exception $e) {
        $err = "error in username check using ajax call" . $e->getMessage();
    }
    $response = "<span style='color: green;'>Available.</span>";
    if (mysqli_num_rows($result)) {
        $row = mysqli_fetch_array($result);

        $count = $row['Id'];

        if ($count > 0) {
            $response = "<span style='color: red;'>Not Available.</span>";
        }
    }

    echo $response;
    die;
}
