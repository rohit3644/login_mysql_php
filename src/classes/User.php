<?php

/*  Class for Login Validation and Registering new User */



session_start();
class User
{

    public function __construct()
    {
    }

    // Validating Login 

    public function validate($username, $password, $obj, $mysqli)
    {
        $result = 0;
        //if username or password field is empty echo below statement
        if (empty($username) || empty($password)) {
            $result = 0;
        } else {
            $password_hash = hash('sha256', $password);
            $data = [
                $tables_array = ["Login"],
                $select_columns_array = ["Id"],
                $where_condition_array = ["UserName" => $username, "Password" => $password_hash],
                $operator_condition_array = ["AND"]
            ];
            // Validating username and password
            try {
                $result = $obj->select($data, false);
            } catch (Exception $e) {
                $err = "error in login" . $e->getMessage();
            }
            if (mysqli_num_rows($result) == 1) {
                $result = 1;
            } else {
                $result = -1;
            }
        }
        return $result;
    }


    // Registering new Users

    public function registration($username, $password, $confirm_password, $obj, $mysqli)
    {
        $result = 0;
        if ($password != $confirm_password) {
            $result = -1;
        } else {

            //if username or password field is empty echo below statement
            if (empty($username) || empty($password) || empty($confirm_password)) {
                $result = 0;
            } else {
                $password_hash = hash('sha256', $password);
                $data = [
                    $tables_array = ["Login"],
                    $select_columns_array = ["Id"],
                    $where_condition_array = ["UserName" => $username, "Password" => $password_hash],
                    $special_condition_array = ["AND"]
                ];
                // Checking if the user already exists
                try {
                    $result = $obj->select($data, false);
                } catch (Exception $e) {
                    $err = "error in registration" . $e->getMessage();
                }
                $data_array = ["UserName" => $username, "Password" => $password_hash];
                if (mysqli_num_rows($result) < 1) {
                    // If the details are unique registering the new user
                    $table = "Login";
                    try {
                        $res = $obj->insert($table, $data_array, false);
                    } catch (Exception $e) {
                        $err = "error in inserting data for registration" . $e->getMessage();
                    }
                    $result = $res;
                } else {
                    $result = -2;
                }
            }
        }
        return $result;
    }
}
