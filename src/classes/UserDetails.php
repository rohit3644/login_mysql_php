<?php

/* class for validating, inserting and displaying information */


session_start();
class User
{
    public function __construct()
    {
    }

    // validate if all the form fields are filled correctly

    public function valid_data($details, $error_count, $obj, $mysqli)
    {
        // Validate if age is between 20 and 30
        if ($details["Age"] >= 20 && $details["Age"] <= 30) {
            $age = $details["Age"];
        } else {
            $error_count++;
            $msg = " * Please enter a valid age ";
        }
        $resume_type_tmp = mime_content_type($details["Resume"]["tmp_name"]);
        $photo_type_temp = mime_content_type($details["Picture"]["tmp_name"]);
        $file_store = "upload/" . $details["Picture"]["name"];
        $checked_arr = $details["Skills"];
        $skillcount = count($checked_arr);
        // Check if the skill count is 2 or more
        if ($skillcount < 2) {
            $error_count++;
            $skillmsgerror = "* Please enter atleast 2 skills";
        } else {
            $skillmsg = "";
            foreach ($checked_arr as $skill) {
                $skillmsg = $skillmsg . " " . $skill;
            }
        }

        $photo_array = ["jpg", "png", "jpeg"];
        $index = strpos($photo_type_temp, "/");
        $photo_type = substr($photo_type_temp, $index + 1);
        // Check if image is of correct type and size is less than equal to 1 MB
        if (!in_array($photo_type, $photo_array) || $details["Picture"]["size"] > 1001718) {
            $error_count++;
            $photomsg = "* Please upload the image in jpg,png and jpeg format only and of size less than 1 MB";
        } else {

            if (move_uploaded_file($details["Picture"]["tmp_name"], $file_store)) {
                $photo_status =  "File is valid, and was successfully uploaded";
            } else {
                $error_count++;
                $photo_status = "Upload failed" . "<br>";
            }
        }

        $resume_name = $details["Resume"]["name"];
        $resume_index = strpos($resume_type_tmp, "/");
        $resume_type = substr($resume_type_tmp, $resume_index + 1);
        // Check if resume is of correct type and size is less than equal to 2 MB
        if (!($resume_type == "pdf") || $details["Resume"]["size"] > 2003436) {
            $error_count++;
            $resume_msg = "* Please upload the resume file in pdf format only and of size less than 2 MB";
        } else {
            // Moving uploaded files
            if (move_uploaded_file($details["Resume"]["tmp_name"], "upload/" . $details["Resume"]["name"])) {
                $resume_status =  "File is valid, and was successfully uploaded";
            } else {
                $error_count++;
                $resume_status = "Upload failed" . "<br>";
            }
        }

        return compact("msg", "skillmsgerror", "photomsg", "photo_status", "resume_msg", "resume_status", "error_count", "checked_arr", "file_store");
    }

    // After validation informations are inserted into the database

    public function insert($details, $checked_arr, $obj, $mysqli)
    {
        $result = 0;
        $data = [
            $tables_array = ["Users"],
            $select_column_array = ["Id"],
            $where_condition_array = ["Name" => $details["Name"], "Email" => $details["Email"]],
            $special_condition_array = ["AND"]
        ];
        // Check if the User already exists
        try {
            $result = $obj->select($data, false);
        } catch (Exception $e) {
            $err = "error in existing user check in profile section" . $e->getMessage();
        }
        // If the details are Unique inserting into Users and UsersSkills table
        if (mysqli_num_rows($result) < 1) {
            $table = "Users";
            $data_array = ["Name" => $details["Name"], "Email" => $details["Email"], "MobileNo" => $details["MobileNo"], "PictureName" => $details["Picture"]["name"], "Age" => $details["Age"], "State" => $details["State"], "Gender" => $details["Gender"], "ResumeName" => $details["Resume"]["name"]];
            try {
                $res = $obj->insert($table, $data_array, true);
            } catch (Exception $e) {
                $err = "error in inserting User details in profile section" . $e->getMessage();
            }
            foreach ($checked_arr as $SkillId) {
                $table = "UserSkills";
                $data_array = ["UserId" => $res, "SkillsId" => $SkillId];
                try {
                    $result = $obj->insert($table, $data_array, false);
                } catch (Exception $e) {
                    $err = "error in inserting int UserSkills in profile section" . $e->getMessage();
                }
            }
            $result = $res;
        } else {
            $result = -1;
        }
        return $result;
    }

    // Displaying user information on the page

    public function display($details, $checked_arr, Database $obj, $mysqli)
    {
        $name = $email = $mobileno = $picturename = $age = $state = $gender = $resumename = $skills = "";
        $total_skills = "";
        $data = [
            $tables_array = ["Users"],
            $select_column_array = ["Name", "Email", "MobileNo", "PictureName", "Age", "State", "Gender", "ResumeName"],
            $where_condition_array = ["Email" => $details["Email"]],
            $special_condition_array = []
        ];
        // Getting user information from the database for displaying
        try {
            $query = $obj->select($data, true);
        } catch (Exception $e) {
            $err = "error in selecting data from profile section" . $e->getMessage();
        }
        $query->store_result();
        $query->bind_result($name, $email, $mobileno, $picturename, $age, $state, $gender, $resumename);
        $query->fetch();

        $data1 = [
            $tables_array1 = ["Users", "UserSkills", "Skills"],
            $select_column_array = ["SkillName"],
            $where_condition_array = ["Skills.Id" => "UserSkills.SkillsId", "Users.Id" => "UserSkills.UserId", "Users.Name" => "'" . $details["Name"] . "'"],
            $special_condition_array = ["AND", "AND"]
        ];
        // Getting Skills of the user from the database for displaying
        try {
            $sql = $obj->select($data1, true, "Join");
            $result = $sql->get_result();
            while ($row = $result->fetch_assoc()) {
                $total_skills  = $total_skills . " " . $row['SkillName'];
            }
        } catch (Exception $e) {
            $err = "error in selecting skills from profile section" . $e->getMessage();
        }


        return compact("name", "email", "mobileno", "picturename", "age", "state", "gender", "resumename", "total_skills");
    }
}
