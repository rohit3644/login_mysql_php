<?php

/* User enters his information and information is displayed on screen */


//header
require_once("header.php");

session_start();
require_once("src/classes/UserDetails.php");
require_once("src/database/database.php");
$obj = Database::getInstance();
$mysqli = $obj->getConnection();

// Validating access without login
if (!isset($_SESSION['access'])) {

    header('Location: index.php');

    exit();
}



// array for state drop-down
$state_ar = ["Odisha", "Jharkhand", "Bihar", "West Bengal", "Assam"];
$skill_ar = ["Java", "PHP", "Python", "Linux", "Django"];


// On form Submit
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $error_count = 0;

    $details = [
        "Name" => $_POST["name"],
        "Email" => $_POST["email"],
        "MobileNo" => $_POST["mobile"],
        "Picture" => $_FILES["photo"],
        "Age" => $_POST["age"],
        "State" =>  $_POST["state"],
        "Gender" => $_POST["gender"],
        "Resume" => $_FILES["resume"],
        "Skills" => $_POST['checkbox']
    ];

    $userobj = new User();
    $result = $userobj->valid_data($details, $error_count, $obj, $mysqli);


    $msg = $result["msg"];
    $skillmsgerror = $result["skillmsgerror"];
    $photomsg = $result["photomsg"];
    $photo_status = $result["photo_status"];
    $resume_msg = $result["resume_msg"];
    $resume_status = $result["resume_status"];
    $error_count = $result["error_count"];
    $checked_arr = $result["checked_arr"];
    $file_store = $result["file_store"];
    $exist_count = 0;
    if ($error_count == 0) {

        if ($userobj->insert($details, $checked_arr, $obj, $mysqli) == -1) {
            $exist_count++;
        } else {

            $userresult = $userobj->display($details, $checked_arr, $obj, $mysqli);
            $name = $userresult["name"];
            $email = $userresult["email"];
            $mobileno = $userresult["mobileno"];
            $picturename = $userresult["picturename"];
            $age = $userresult["age"];
            $state = $userresult["state"];
            $gender = $userresult["gender"];
            $resumename = $userresult["resumename"];
            $total_skills = $userresult["total_skills"];
        }
    }
}

mysqli_close($mysqli);
?>

<div class="main-content-profile">
    <div class="container">
        <div class="row">
            <div class="col-6">
                <div class="jumbotron jumbotron-fluid">
                    <div class="profile">
                        <form method="post" enctype="multipart/form-data">
                            <strong>
                                <p>Name : <input type="text" name="name" id="name" required></p>
                                <p id="name_error"></p>
                                <p>Email : <input type="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2, 4}$" id="email" required></p>
                                <p id="email_error"></p>
                                <p>Mobile No.: <input type="tel" name="mobile" maxlength="10" required></p>
                                <p>Profile Photo: <input type="file" name="photo" width="20px" class="btn"> <b><?php echo $photomsg . "<br />"; ?></b>
                                </p>

                                <p>Gender: <select name="gender">
                                        <option value="Male" selected>Male</option>
                                        <option value="Female">Female</option>
                                    </select></p>
                                <p>State: <select name="state" id="state" class="form-control">
                                        <?php
                                        foreach ($state_ar as $name1) {
                                        ?>
                                            <option value="<?php echo $name1 ?>"><?php echo $name1 ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select></p>

                                <p>Skills:
                                    Java <input type="checkbox" value="1" name="checkbox[]">
                                    PHP <input type="checkbox" value="2" name="checkbox[]">
                                    Python <input type="checkbox" value="3" name="checkbox[]">
                                    Linux <input type="checkbox" value="4" name="checkbox[]">
                                    Django <input type="checkbox" value="5" name="checkbox[]"> <b><?php echo $skillmsgerror ?></b></p>

                                <p>Age: <input type="number" name="age" id="age" required> <b><?php echo $msg ?></b></p>
                                <p id="age_error"></p>
                                <p>Resume: <input type="file" name="resume" class="btn"> <b><?php echo $resume_msg . "\n" ?></b>
                                </p>

                                <input type="submit" value="Login" name="login">
                        </form>
                    </div>
                    <b>
                </div>
            </div>
            </strong>
            <div class="col-6">
                <div class="display" align="center">

                    <!-- Displaying Information -->
                    <?php
                    if ($_SERVER["REQUEST_METHOD"] == "POST") {
                        if ($exist_count > 0) { ?>
                            <div class="alert alert-danger" role="alert">
                                <h1>User Exists!</h1>
                            </div>
                        <?php
                        } else {
                        ?><div class="jumbotron jumbotron-fluid">
                                <?php echo "<br>"; ?>
                                <img src="<?= $file_store ?>" alt="Profile Picture" height="200px" width="200px"><br />
                        <?php
                            echo "<br>";
                            echo "Name: " . $name . "<br>";
                            echo "Email: " . $email . "<br>";
                            echo "Mobile Number: " . $mobileno . "<br>";
                            echo "Gender: " . $gender . "<br>";
                            echo "State: " . $state . "<br>";
                            echo "Age: " . $age . "<br>";
                            echo "Skills: " . $total_skills . "<br>";
                            echo "Image Name: " . $picturename . "<br>";
                            echo "Resume Name: " . $resumename . "<br>";
                        }
                    }
                        ?>
                            </div>
                            </b>
                </div>
            </div>
        </div>
    </div>
</div>

<?php require_once("footer.php"); ?>